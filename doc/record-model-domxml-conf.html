<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><title>3. DOM Record Model Configuration</title><meta name="generator" content="DocBook XSL Stylesheets Vsnapshot"><link rel="home" href="index.html" title="Zebra - User's Guide and Reference"><link rel="up" href="record-model-domxml.html" title="Chapter 7. DOM XML Record Model and Filter Module"><link rel="prev" href="record-model-domxml-pipeline.html" title="2. DOM XML filter pipeline configuration"><link rel="next" href="record-model-alvisxslt.html" title="Chapter 8. ALVIS XML Record Model and Filter Module"></head><body><link rel="stylesheet" type="text/css" href="common/style1.css"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">3. <acronym class="acronym">DOM</acronym> Record Model Configuration</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="record-model-domxml-pipeline.html">Prev</a> </td><th width="60%" align="center">Chapter 7. <acronym class="acronym">DOM</acronym> <acronym class="acronym">XML</acronym> Record Model and Filter Module</th><td width="20%" align="right"> <a accesskey="n" href="record-model-alvisxslt.html">Next</a></td></tr></table><hr></div><div class="section"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="record-model-domxml-conf"></a>3. <acronym class="acronym">DOM</acronym> Record Model Configuration</h2></div></div></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="record-model-domxml-index"></a>3.1. <acronym class="acronym">DOM</acronym> Indexing Configuration</h3></div></div></div><p>
     As mentioned above, there can be only one indexing pipeline,
     and configuration of the indexing process is a synonym
     of writing an <acronym class="acronym">XSLT</acronym> stylesheet which produces <acronym class="acronym">XML</acronym> output containing the
     magic processing instructions or elements discussed in
     <a class="xref" href="record-model-domxml-pipeline.html#record-model-domxml-canonical-index" title="2.5. Canonical Indexing Format">Section 2.5, &#8220;Canonical Indexing Format&#8221;</a>.
     Obviously, there are million of different ways to accomplish this
     task, and some comments and code snippets are in order to
     enlighten the wary.
    </p><p>
     Stylesheets can be written in the <span class="emphasis"><em>pull</em></span> or
     the <span class="emphasis"><em>push</em></span> style: <span class="emphasis"><em>pull</em></span>
     means that the output <acronym class="acronym">XML</acronym> structure is taken as starting point of
     the internal structure of the <acronym class="acronym">XSLT</acronym> stylesheet, and portions of
     the input <acronym class="acronym">XML</acronym> are <span class="emphasis"><em>pulled</em></span> out and inserted
     into the right spots of the output <acronym class="acronym">XML</acronym> structure.
     On the other
     side, <span class="emphasis"><em>push</em></span> <acronym class="acronym">XSLT</acronym> stylesheets are recursively
     calling their template definitions, a process which is commanded
     by the input <acronym class="acronym">XML</acronym> structure, and is triggered to produce
     some output <acronym class="acronym">XML</acronym>
     whenever some special conditions in the input stylesheets are
     met. The <span class="emphasis"><em>pull</em></span> type is well-suited for input
     <acronym class="acronym">XML</acronym> with strong and well-defined structure and semantics, like the
     following <acronym class="acronym">OAI</acronym> indexing example, whereas the
     <span class="emphasis"><em>push</em></span> type might be the only possible way to
     sort out deeply recursive input <acronym class="acronym">XML</acronym> formats.
    </p><p>
     A <span class="emphasis"><em>pull</em></span> stylesheet example used to index
     <acronym class="acronym">OAI</acronym> harvested records could use some of the following template
     definitions:
     </p><pre class="screen">
      
      &lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:z="http://indexdata.com/zebra-2.0"
      xmlns:oai="http://www.openarchives.org/&amp;acro.oai;/2.0/"
      xmlns:oai_dc="http://www.openarchives.org/&amp;acro.oai;/2.0/oai_dc/"
      xmlns:dc="http://purl.org/dc/elements/1.1/"
      version="1.0"&gt;

      &lt;!-- Example pull and magic element style Zebra indexing --&gt;
      &lt;xsl:output indent="yes" method="xml" version="1.0" encoding="UTF-8"/&gt;

      &lt;!-- disable all default text node output --&gt;
      &lt;xsl:template match="text()"/&gt;

      &lt;!-- disable all default recursive element node transversal --&gt;
      &lt;xsl:template match="node()"/&gt;

      &lt;!-- match only on oai xml record root --&gt;
      &lt;xsl:template match="/"&gt;
      &lt;z:record z:id="{normalize-space(oai:record/oai:header/oai:identifier)}"&gt;
      &lt;!-- you may use z:rank="{some XSLT; function here}" --&gt;

      &lt;!-- explicetly calling defined templates --&gt;
      &lt;xsl:apply-templates/&gt;
     &lt;/z:record&gt;
     &lt;/xsl:template&gt;

      &lt;!-- OAI indexing templates --&gt;
      &lt;xsl:template match="oai:record/oai:header/oai:identifier"&gt;
      &lt;z:index name="oai_identifier:0"&gt;
      &lt;xsl:value-of select="."/&gt;
     &lt;/z:index&gt;
     &lt;/xsl:template&gt;

      &lt;!-- etc, etc --&gt;

      &lt;!-- DC specific indexing templates --&gt;
      &lt;xsl:template match="oai:record/oai:metadata/oai_dc:dc/dc:title"&gt;
      &lt;z:index name="dc_any:w dc_title:w dc_title:p dc_title:s "&gt;
      &lt;xsl:value-of select="."/&gt;
     &lt;/z:index&gt;
     &lt;/xsl:template&gt;

      &lt;!-- etc, etc --&gt;

     &lt;/xsl:stylesheet&gt;
      
     </pre><p>
    </p></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="record-model-domxml-index-marc"></a>3.2. <acronym class="acronym">DOM</acronym> Indexing <acronym class="acronym">MARCXML</acronym></h3></div></div></div><p>
     The <acronym class="acronym">DOM</acronym> filter allows indexing of both binary <acronym class="acronym">MARC</acronym> records
     and <acronym class="acronym">MARCXML</acronym> records, depending on its configuration.
     A typical <acronym class="acronym">MARCXML</acronym> record might look like this:
     </p><pre class="screen">
      
      &lt;record xmlns="http://www.loc.gov/MARC21/slim"&gt;
      &lt;rank&gt;42&lt;/rank&gt;
      &lt;leader&gt;00366nam  22001698a 4500&lt;/leader&gt;
      &lt;controlfield tag="001"&gt;   11224466   &lt;/controlfield&gt;
      &lt;controlfield tag="003"&gt;DLC  &lt;/controlfield&gt;
      &lt;controlfield tag="005"&gt;00000000000000.0  &lt;/controlfield&gt;
      &lt;controlfield tag="008"&gt;910710c19910701nju           00010 eng    &lt;/controlfield&gt;
      &lt;datafield tag="010" ind1=" " ind2=" "&gt;
      &lt;subfield code="a"&gt;   11224466 &lt;/subfield&gt;
     &lt;/datafield&gt;
      &lt;datafield tag="040" ind1=" " ind2=" "&gt;
      &lt;subfield code="a"&gt;DLC&lt;/subfield&gt;
      &lt;subfield code="c"&gt;DLC&lt;/subfield&gt;
     &lt;/datafield&gt;
      &lt;datafield tag="050" ind1="0" ind2="0"&gt;
      &lt;subfield code="a"&gt;123-xyz&lt;/subfield&gt;
     &lt;/datafield&gt;
      &lt;datafield tag="100" ind1="1" ind2="0"&gt;
      &lt;subfield code="a"&gt;Jack Collins&lt;/subfield&gt;
     &lt;/datafield&gt;
      &lt;datafield tag="245" ind1="1" ind2="0"&gt;
      &lt;subfield code="a"&gt;How to program a computer&lt;/subfield&gt;
     &lt;/datafield&gt;
      &lt;datafield tag="260" ind1="1" ind2=" "&gt;
      &lt;subfield code="a"&gt;Penguin&lt;/subfield&gt;
     &lt;/datafield&gt;
      &lt;datafield tag="263" ind1=" " ind2=" "&gt;
      &lt;subfield code="a"&gt;8710&lt;/subfield&gt;
     &lt;/datafield&gt;
      &lt;datafield tag="300" ind1=" " ind2=" "&gt;
      &lt;subfield code="a"&gt;p. cm.&lt;/subfield&gt;
     &lt;/datafield&gt;
     &lt;/record&gt;
      
     </pre><p>
    </p><p>
     It is easily possible to make string manipulation in the <acronym class="acronym">DOM</acronym>
     filter. For example, if you want to drop some leading articles
     in the indexing of sort fields, you might want to pick out the
     <acronym class="acronym">MARCXML</acronym> indicator attributes to chop of leading substrings. If
     the above <acronym class="acronym">XML</acronym> example would have an indicator
     <code class="literal">ind2="8"</code> in the title field
     <code class="literal">245</code>, i.e.
     </p><pre class="screen">
      
      &lt;datafield tag="245" ind1="1" ind2="8"&gt;
      &lt;subfield code="a"&gt;How to program a computer&lt;/subfield&gt;
     &lt;/datafield&gt;
      
     </pre><p>
     one could write a template taking into account this information
     to chop the first <code class="literal">8</code> characters from the
     sorting index <code class="literal">title:s</code> like this:
     </p><pre class="screen">
      
      &lt;xsl:template match="m:datafield[@tag='245']"&gt;
      &lt;xsl:variable name="chop"&gt;
      &lt;xsl:choose&gt;
      &lt;xsl:when test="not(number(@ind2))"&gt;0&lt;/xsl:when&gt;
      &lt;xsl:otherwise&gt;&lt;xsl:value-of select="number(@ind2)"/&gt;&lt;/xsl:otherwise&gt;
     &lt;/xsl:choose&gt;
     &lt;/xsl:variable&gt;

      &lt;z:index name="title:w title:p any:w"&gt;
      &lt;xsl:value-of select="m:subfield[@code='a']"/&gt;
     &lt;/z:index&gt;

      &lt;z:index name="title:s"&gt;
      &lt;xsl:value-of select="substring(m:subfield[@code='a'], $chop)"/&gt;
     &lt;/z:index&gt;

     &lt;/xsl:template&gt;
      
     </pre><p>
     The output of the above <acronym class="acronym">MARCXML</acronym> and <acronym class="acronym">XSLT</acronym> excerpt would then be:
     </p><pre class="screen">
      
      &lt;z:index name="title:w title:p any:w"&gt;How to program a computer&lt;/z:index&gt;
      &lt;z:index name="title:s"&gt;program a computer&lt;/z:index&gt;
      
     </pre><p>
     and the record would be sorted in the title index under 'P', not 'H'.
    </p></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="record-model-domxml-index-wizzard"></a>3.3. <acronym class="acronym">DOM</acronym> Indexing Wizardry</h3></div></div></div><p>
     The names and types of the indexes can be defined in the
     indexing <acronym class="acronym">XSLT</acronym> stylesheet <span class="emphasis"><em>dynamically according to
      content in the original <acronym class="acronym">XML</acronym> records</em></span>, which has
     opportunities for great power and wizardry as well as grande
     disaster.
    </p><p>
     The following excerpt of a <span class="emphasis"><em>push</em></span> stylesheet
     <span class="emphasis"><em>might</em></span>
     be a good idea according to your strict control of the <acronym class="acronym">XML</acronym>
     input format (due to rigorous checking against well-defined and
     tight RelaxNG or <acronym class="acronym">XML</acronym> Schema's, for example):
     </p><pre class="screen">
      
      &lt;xsl:template name="element-name-indexes"&gt;
      &lt;z:index name="{name()}:w"&gt;
      &lt;xsl:value-of select="'1'"/&gt;
     &lt;/z:index&gt;
     &lt;/xsl:template&gt;
      
     </pre><p>
     This template creates indexes which have the name of the working
     node of any input  <acronym class="acronym">XML</acronym> file, and assigns a '1' to the index.
     The example query
     <code class="literal">find @attr 1=xyz 1</code>
     finds all files which contain at least one
     <code class="literal">xyz</code> <acronym class="acronym">XML</acronym> element. In case you can not control
     which element names the input files contain, you might ask for
     disaster and bad karma using this technique.
    </p><p>
     One variation over the theme <span class="emphasis"><em>dynamically created
      indexes</em></span> will definitely be unwise:
     </p><pre class="screen">
      
      &lt;!-- match on oai xml record root --&gt;
      &lt;xsl:template match="/"&gt;
      &lt;z:record&gt;

      &lt;!-- create dynamic index name from input content --&gt;
      &lt;xsl:variable name="dynamic_content"&gt;
      &lt;xsl:value-of select="oai:record/oai:header/oai:identifier"/&gt;
     &lt;/xsl:variable&gt;

      &lt;!-- create zillions of indexes with unknown names --&gt;
      &lt;z:index name="{$dynamic_content}:w"&gt;
      &lt;xsl:value-of select="oai:record/oai:metadata/oai_dc:dc"/&gt;
     &lt;/z:index&gt;
     &lt;/z:record&gt;

     &lt;/xsl:template&gt;
      
     </pre><p>
     Don't be tempted to play too smart tricks with the power of
     <acronym class="acronym">XSLT</acronym>, the above example will create zillions of
     indexes with unpredictable names, resulting in severe <span class="application">Zebra</span>
     index pollution..
    </p></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="record-model-domxml-debug"></a>3.4. Debuggig <acronym class="acronym">DOM</acronym> Filter Configurations</h3></div></div></div><p>
     It can be very hard to debug a <acronym class="acronym">DOM</acronym> filter setup due to the many
     successive <acronym class="acronym">MARC</acronym> syntax translations, <acronym class="acronym">XML</acronym> stream splitting and
     <acronym class="acronym">XSLT</acronym> transformations involved. As an aid, you have always the
     power of the <code class="literal">-s</code> command line switch to the
     <code class="literal">zebraidz</code> indexing command at your hand:
     </p><pre class="screen">
      zebraidx -s -c zebra.cfg update some_record_stream.xml
     </pre><p>
     This command line simulates indexing and dumps a lot of debug
     information in the logs, telling exactly which transformations
     have been applied, how the documents look like after each
     transformation, and which record ids and terms are send to the indexer.
    </p></div></div><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="record-model-domxml-pipeline.html">Prev</a> </td><td width="20%" align="center"><a accesskey="u" href="record-model-domxml.html">Up</a></td><td width="40%" align="right"> <a accesskey="n" href="record-model-alvisxslt.html">Next</a></td></tr><tr><td width="40%" align="left" valign="top">2. <acronym class="acronym">DOM</acronym> <acronym class="acronym">XML</acronym> filter pipeline configuration </td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right" valign="top"> Chapter 8. ALVIS <acronym class="acronym">XML</acronym> Record Model and Filter Module</td></tr></table></div></body></html>
