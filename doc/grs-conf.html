<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"><title>3. GRS-1 Record Model Configuration</title><meta name="generator" content="DocBook XSL Stylesheets Vsnapshot"><link rel="home" href="index.html" title="Zebra - User's Guide and Reference"><link rel="up" href="grs.html" title="Chapter 9. GRS-1 Record Model and Filter Modules"><link rel="prev" href="grs-internal-representation.html" title="2. GRS-1 Internal Record Representation"><link rel="next" href="grs-exchange-formats.html" title="4. GRS-1 Exchange Formats"></head><body><link rel="stylesheet" type="text/css" href="common/style1.css"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">3. <acronym class="acronym">GRS-1</acronym> Record Model Configuration</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="grs-internal-representation.html">Prev</a> </td><th width="60%" align="center">Chapter 9. <acronym class="acronym">GRS-1</acronym> Record Model and Filter Modules</th><td width="20%" align="right"> <a accesskey="n" href="grs-exchange-formats.html">Next</a></td></tr></table><hr></div><div class="section"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="grs-conf"></a>3. <acronym class="acronym">GRS-1</acronym> Record Model Configuration</h2></div></div></div><p>
    The following sections describe the configuration files that govern
    the internal management of <code class="literal">grs</code> records.
    The system searches for the files
    in the directories specified by the <span class="emphasis"><em>profilePath</em></span>
    setting in the <code class="literal">zebra.cfg</code> file.
   </p><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="grs-abstract-syntax"></a>3.1. The Abstract Syntax</h3></div></div></div><p>
     The abstract syntax definition (also known as an Abstract Record
     Structure, or ARS) is the focal point of the
     record schema description. For a given schema, the ABS file may state any
     or all of the following:
    </p><p>

     </p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
        The object identifier of the <acronym class="acronym">Z39.50</acronym> schema associated
        with the ARS, so that it can be referred to by the client.
       </p></li><li class="listitem"><p>
        The attribute set (which can possibly be a compound of multiple
        sets) which applies in the profile. This is used when indexing and
        searching the records belonging to the given profile.
       </p></li><li class="listitem"><p>
        The tag set (again, this can consist of several different sets).
        This is used when reading the records from a file, to recognize the
        different tags, and when transmitting the record to the client -
        mapping the tags to their numerical representation, if they are
        known.
       </p></li><li class="listitem"><p>
        The variant set which is used in the profile. This provides a
        vocabulary for specifying the <span class="emphasis"><em>forms</em></span> of
        data that appear inside the records.
       </p></li><li class="listitem"><p>
        Element set names, which are a shorthand way for the client to
        ask for a subset of the data elements contained in a record. Element
        set names, in the retrieval module, are mapped to <span class="emphasis"><em>element
         specifications</em></span>, which contain information equivalent to the
        <span class="emphasis"><em>Espec-1</em></span> syntax of <acronym class="acronym">Z39.50</acronym>.
       </p></li><li class="listitem"><p>
        Map tables, which may specify mappings to
        <span class="emphasis"><em>other</em></span> database profiles, if desired.
       </p></li><li class="listitem"><p>
        Possibly, a set of rules describing the mapping of elements to a
        <acronym class="acronym">MARC</acronym> representation.

       </p></li><li class="listitem"><p>
        A list of element descriptions (this is the actual ARS of the
        schema, in <acronym class="acronym">Z39.50</acronym> terms), which lists the ways in which the various
        tags can be used and organized hierarchically.
       </p></li></ul></div><p>

    </p><p>
     Several of the entries above simply refer to other files, which
     describe the given objects.
    </p></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="grs-configuration-files"></a>3.2. The Configuration Files</h3></div></div></div><p>
     This section describes the syntax and use of the various tables which
     are used by the retrieval module.
    </p><p>
     The number of different file types may appear daunting at first, but
     each type corresponds fairly clearly to a single aspect of the <acronym class="acronym">Z39.50</acronym>
     retrieval facilities. Further, the average database administrator,
     who is simply reusing an existing profile for which tables already
     exist, shouldn't have to worry too much about the contents of these tables.
    </p><p>
     Generally, the files are simple ASCII files, which can be maintained
     using any text editor. Blank lines, and lines beginning with a (#) are
     ignored. Any characters on a line followed by a (#) are also ignored.
     All other lines contain <span class="emphasis"><em>directives</em></span>, which provide
     some setting or value to the system.
     Generally, settings are characterized by a single
     keyword, identifying the setting, followed by a number of parameters.
     Some settings are repeatable (r), while others may occur only once in a
     file. Some settings are optional (o), while others again are
     mandatory (m).
    </p></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="abs-file"></a>3.3. The Abstract Syntax (.abs) Files</h3></div></div></div><p>
     The name of this file type is slightly misleading in <acronym class="acronym">Z39.50</acronym> terms,
     since, apart from the actual abstract syntax of the profile, it also
     includes most of the other definitions that go into a database
     profile.
    </p><p>
     When a record in the canonical, <acronym class="acronym">SGML</acronym>-like format is read from a file
     or from the database, the first tag of the file should reference the
     profile that governs the layout of the record. If the first tag of the
     record is, say, <code class="literal">&lt;gils&gt;</code>, the system will look
     for the profile definition in the file <code class="literal">gils.abs</code>.
     Profile definitions are cached, so they only have to be read once
     during the lifespan of the current process.
    </p><p>
     When writing your own input filters, the
     <span class="emphasis"><em>record-begin</em></span> command
     introduces the profile, and should always be called first thing when
     introducing a new record.
    </p><p>
     The file may contain the following directives:
    </p><p>
     </p><div class="variablelist"><dl class="variablelist"><dt><span class="term">name <em class="replaceable"><code>symbolic-name</code></em></span></dt><dd><p>
         (m) This provides a shorthand name or
         description for the profile. Mostly useful for diagnostic purposes.
        </p></dd><dt><span class="term">reference <em class="replaceable"><code>OID-name</code></em></span></dt><dd><p>
         (m) The reference name of the OID for the profile.
         The reference names can be found in the <span class="emphasis"><em>util</em></span>
         module of <span class="application">YAZ</span>.
        </p></dd><dt><span class="term">attset <em class="replaceable"><code>filename</code></em></span></dt><dd><p>
         (m) The attribute set that is used for
         indexing and searching records belonging to this profile.
        </p></dd><dt><span class="term">tagset <em class="replaceable"><code>filename</code></em></span></dt><dd><p>
         (o) The tag set (if any) that describe
         that fields of the records.
        </p></dd><dt><span class="term">varset <em class="replaceable"><code>filename</code></em></span></dt><dd><p>
         (o) The variant set used in the profile.
        </p></dd><dt><span class="term">maptab <em class="replaceable"><code>filename</code></em></span></dt><dd><p>
         (o,r) This points to a
         conversion table that might be used if the client asks for the record
         in a different schema from the native one.
        </p></dd><dt><span class="term">marc <em class="replaceable"><code>filename</code></em></span></dt><dd><p>
         (o) Points to a file containing parameters
         for representing the record contents in the ISO2709 syntax.
         Read the description of the <acronym class="acronym">MARC</acronym> representation facility below.
        </p></dd><dt><span class="term">esetname <em class="replaceable"><code>name filename</code></em></span></dt><dd><p>
         (o,r) Associates the
         given element set name with an element selection file. If an (@) is
         given in place of the filename, this corresponds to a null mapping for
         the given element set name.
        </p></dd><dt><span class="term">all <em class="replaceable"><code>tags</code></em></span></dt><dd><p>
         (o) This directive specifies a list of attributes
         which should be appended to the attribute list given for each
         element. The effect is to make every single element in the abstract
         syntax searchable by way of the given attributes. This directive
         provides an efficient way of supporting free-text searching across all
         elements. However, it does increase the size of the index
         significantly. The attributes can be qualified with a structure, as in
         the <em class="replaceable"><code>elm</code></em> directive below.
        </p></dd><dt><span class="term">elm <em class="replaceable"><code>path name attributes</code></em></span></dt><dd><p>
         (o,r) Adds an element to the abstract record syntax of the schema.
         The <em class="replaceable"><code>path</code></em> follows the
         syntax which is suggested by the <acronym class="acronym">Z39.50</acronym> document - that is, a sequence
         of tags separated by slashes (/). Each tag is given as a
         comma-separated pair of tag type and -value surrounded by parenthesis.
         The <em class="replaceable"><code>name</code></em> is the name of the element, and
         the <em class="replaceable"><code>attributes</code></em>
         specifies which attributes to use when indexing the element in a
         comma-separated list.
         A <code class="literal">!</code> in place of the attribute name is equivalent
	 to specifying an attribute name identical to the element name.
         A <code class="literal">-</code> in place of the attribute name
         specifies that no indexing is to take place for the given element.
         The attributes can be qualified with <em class="replaceable"><code>field
          types</code></em> to specify which
         character set should govern the indexing procedure for that field.
         The same data element may be indexed into several different
         fields, using different character set definitions.
         See the <a class="xref" href="fields-and-charsets.html" title="Chapter 10. Field Structure and Character Sets">Chapter 10, <i>Field Structure and Character Sets
  </i></a>.
         The default field type is <code class="literal">w</code> for
         <span class="emphasis"><em>word</em></span>.
        </p></dd><dt><span class="term">xelm <em class="replaceable"><code>xpath attributes</code></em></span></dt><dd><p>
         Specifies indexing for record nodes given by
         <em class="replaceable"><code>xpath</code></em>. Unlike directive
         elm, this directive allows you to index attribute
         contents. The <em class="replaceable"><code>xpath</code></em> uses
         a syntax similar to XPath. The <em class="replaceable"><code>attributes</code></em>
         have same syntax and meaning as directive elm, except that operator
         ! refers to the nodes selected by <em class="replaceable"><code>xpath</code></em>.
         
        </p></dd><dt><span class="term">melm <em class="replaceable"><code>field$subfield attributes</code></em></span></dt><dd><p>
	 This directive is specifically for <acronym class="acronym">MARC</acronym>-formatted records,
	 ingested either in the form of <acronym class="acronym">MARCXML</acronym> documents, or in the
	 ISO2709/Z39.2 format using the grs.marcxml input filter. You can
	 specify indexing rules for any subfield, or you can leave off the
	 <em class="replaceable"><code>$subfield</code></em> part and specify default rules
	 for all subfields of the given field (note: default rules should come
	 after any subfield-specific rules in the configuration file). The
	 <em class="replaceable"><code>attributes</code></em> have the same syntax and meaning
	 as for the 'elm' directive above.
	</p></dd><dt><span class="term">encoding <em class="replaceable"><code>encodingname</code></em></span></dt><dd><p>
         This directive specifies character encoding for external records.
         For records such as <acronym class="acronym">XML</acronym> that specifies encoding within the
         file via a header this directive is ignored.
         If neither this directive is given, nor an encoding is set
         within external records, ISO-8859-1 encoding is assumed.
	</p></dd><dt><span class="term">xpath <code class="literal">enable</code>/<code class="literal">disable</code></span></dt><dd><p>
         If this directive is followed by <code class="literal">enable</code>,
         then extra indexing is performed to allow for XPath-like queries.
         If this directive is not specified - equivalent to
         <code class="literal">disable</code> - no extra XPath-indexing is performed.
        </p></dd><dt><span class="term">
        systag
        <em class="replaceable"><code>systemTag</code></em>
        <em class="replaceable"><code>actualTag</code></em>
       </span></dt><dd><p>
	 Specifies what information, if any, <span class="application">Zebra</span> should
	 automatically include in retrieval records for the
	 ``system fields'' that it supports.
	 <em class="replaceable"><code>systemTag</code></em> may
	 be any of the following:
	 </p><div class="variablelist"><dl class="variablelist"><dt><span class="term"><code class="literal">rank</code></span></dt><dd><p>
	     An integer indicating the relevance-ranking score
	     assigned to the record.
	    </p></dd><dt><span class="term"><code class="literal">sysno</code></span></dt><dd><p>
	     An automatically generated identifier for the record,
	     unique within this database.  It is represented by the
	     <code class="literal">&lt;localControlNumber&gt;</code> element in
	     <acronym class="acronym">XML</acronym> and the <code class="literal">(1,14)</code> tag in <acronym class="acronym">GRS-1</acronym>.
	    </p></dd><dt><span class="term"><code class="literal">size</code></span></dt><dd><p>
	     The size, in bytes, of the retrieved record.
	    </p></dd></dl></div><p>
	</p><p>
	 The <em class="replaceable"><code>actualTag</code></em> parameter may be
	 <code class="literal">none</code> to indicate that the named element
	 should be omitted from retrieval records.
	</p></dd></dl></div><p>
    </p><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><h3 class="title">Note</h3><p>
      The mechanism for controlling indexing is not adequate for
      complex databases, and will probably be moved into a separate
      configuration table eventually.
     </p></div><p>
     The following is an excerpt from the abstract syntax file for the GILS
     profile.
    </p><p>

     </p><pre class="screen">
      name gils
      reference GILS-schema
      attset gils.att
      tagset gils.tag
      varset var1.var

      maptab gils-usmarc.map

      # Element set names

      esetname VARIANT gils-variant.est  # for WAIS-compliance
      esetname B gils-b.est
      esetname G gils-g.est
      esetname F @

      elm (1,10)               rank                        -
      elm (1,12)               url                         -
      elm (1,14)               localControlNumber     Local-number
      elm (1,16)               dateOfLastModification Date/time-last-modified
      elm (2,1)                title                       w:!,p:!
      elm (4,1)                controlIdentifier      Identifier-standard
      elm (2,6)                abstract               Abstract
      elm (4,51)               purpose                     !
      elm (4,52)               originator                  -
      elm (4,53)               accessConstraints           !
      elm (4,54)               useConstraints              !
      elm (4,70)               availability                -
      elm (4,70)/(4,90)        distributor                 -
      elm (4,70)/(4,90)/(2,7)  distributorName             !
      elm (4,70)/(4,90)/(2,10) distributorOrganization     !
      elm (4,70)/(4,90)/(4,2)  distributorStreetAddress    !
      elm (4,70)/(4,90)/(4,3)  distributorCity             !
     </pre><p>

    </p></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="attset-files"></a>3.4. The Attribute Set (.att) Files</h3></div></div></div><p>
     This file type describes the <em class="replaceable"><code>Use</code></em> elements of
     an attribute set.
     It contains the following directives.
    </p><p>
     </p><div class="variablelist"><dl class="variablelist"><dt><span class="term">name <em class="replaceable"><code>symbolic-name</code></em></span></dt><dd><p>
         (m) This provides a shorthand name or
         description for the attribute set.
         Mostly useful for diagnostic purposes.
        </p></dd><dt><span class="term">reference <em class="replaceable"><code>OID-name</code></em></span></dt><dd><p>
         (m) The reference name of the OID for
         the attribute set.
         The reference names can be found in the <em class="replaceable"><code>util</code></em>
         module of <em class="replaceable"><code><span class="application">YAZ</span></code></em>.
        </p></dd><dt><span class="term">include <em class="replaceable"><code>filename</code></em></span></dt><dd><p>
         (o,r) This directive is used to
         include another attribute set as a part of the current one. This is
         used when a new attribute set is defined as an extension to another
         set. For instance, many new attribute sets are defined as extensions
         to the <em class="replaceable"><code>bib-1</code></em> set.
         This is an important feature of the retrieval
         system of <acronym class="acronym">Z39.50</acronym>, as it ensures the highest possible level of
         interoperability, as those access points of your database which are
         derived from the external set (say, bib-1) can be used even by clients
         who are unaware of the new set.
        </p></dd><dt><span class="term">att
        <em class="replaceable"><code>att-value att-name [local-value]</code></em></span></dt><dd><p>
         (o,r) This
         repeatable directive introduces a new attribute to the set. The
         attribute value is stored in the index (unless a
         <em class="replaceable"><code>local-value</code></em> is
         given, in which case this is stored). The name is used to refer to the
         attribute from the <em class="replaceable"><code>abstract syntax</code></em>.
        </p></dd></dl></div><p>
    </p><p>
     This is an excerpt from the GILS attribute set definition.
     Notice how the file describing the <span class="emphasis"><em>bib-1</em></span>
     attribute set is referenced.
    </p><p>

     </p><pre class="screen">
      name gils
      reference GILS-attset
      include bib1.att

      att 2001		distributorName
      att 2002		indextermsControlled
      att 2003		purpose
      att 2004		accessConstraints
      att 2005		useConstraints
     </pre><p>

    </p></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="grs-tag-files"></a>3.5. The Tag Set (.tag) Files</h3></div></div></div><p>
     This file type defines the tagset of the profile, possibly by
     referencing other tag sets (most tag sets, for instance, will include
     tagsetG and tagsetM from the <acronym class="acronym">Z39.50</acronym> specification. The file may
     contain the following directives.
    </p><p>
     </p><div class="variablelist"><dl class="variablelist"><dt><span class="term">name <span class="emphasis"><em>symbolic-name</em></span></span></dt><dd><p>
         (m) This provides a shorthand name or
         description for the tag set. Mostly useful for diagnostic purposes.
        </p></dd><dt><span class="term">reference <span class="emphasis"><em>OID-name</em></span></span></dt><dd><p>
         (o) The reference name of the OID for the tag set.
         The reference names can be found in the <span class="emphasis"><em>util</em></span>
         module of <span class="emphasis"><em><span class="application">YAZ</span></em></span>.
         The directive is optional, since not all tag sets
         are registered outside of their schema.
        </p></dd><dt><span class="term">type <span class="emphasis"><em>integer</em></span></span></dt><dd><p>
         (m) The type number of the tagset within the schema
         profile (note: this specification really should belong to the .abs
         file. This will be fixed in a future release).
        </p></dd><dt><span class="term">include <span class="emphasis"><em>filename</em></span></span></dt><dd><p>
         (o,r) This directive is used
         to include the definitions of other tag sets into the current one.
        </p></dd><dt><span class="term">tag <span class="emphasis"><em>number names type</em></span></span></dt><dd><p>
         (o,r) Introduces a new tag to the set.
         The <span class="emphasis"><em>number</em></span> is the tag number as used
         in the protocol (there is currently no mechanism for
         specifying string tags at this point, but this would be quick
         work to add).
         The <span class="emphasis"><em>names</em></span> parameter is a list of names
         by which the tag should be recognized in the input file format.
         The names should be separated by slashes (/).
         The <span class="emphasis"><em>type</em></span> is the recommended data type of
         the tag.
         It should be one of the following:

         </p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
            structured
           </p></li><li class="listitem"><p>
            string
           </p></li><li class="listitem"><p>
            numeric
           </p></li><li class="listitem"><p>
            bool
           </p></li><li class="listitem"><p>
            oid
           </p></li><li class="listitem"><p>
            generalizedtime
           </p></li><li class="listitem"><p>
            intunit
           </p></li><li class="listitem"><p>
            int
           </p></li><li class="listitem"><p>
            octetstring
           </p></li><li class="listitem"><p>
            null
           </p></li></ul></div><p>

        </p></dd></dl></div><p>
    </p><p>
     The following is an excerpt from the TagsetG definition file.
    </p><p>
     </p><pre class="screen">
      name tagsetg
      reference TagsetG
      type 2

      tag	1	title		string
      tag	2	author		string
      tag	3	publicationPlace string
      tag	4	publicationDate	string
      tag	5	documentId	string
      tag	6	abstract	string
      tag	7	name		string
      tag	8	date		generalizedtime
      tag	9	bodyOfDisplay	string
      tag	10	organization	string
     </pre><p>
    </p></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="grs-var-files"></a>3.6. The Variant Set (.var) Files</h3></div></div></div><p>
     The variant set file is a straightforward representation of the
     variant set definitions associated with the protocol. At present, only
     the <span class="emphasis"><em>Variant-1</em></span> set is known.
    </p><p>
     These are the directives allowed in the file.
    </p><p>
     </p><div class="variablelist"><dl class="variablelist"><dt><span class="term">name <span class="emphasis"><em>symbolic-name</em></span></span></dt><dd><p>
         (m) This provides a shorthand name or
         description for the variant set. Mostly useful for diagnostic purposes.
        </p></dd><dt><span class="term">reference <span class="emphasis"><em>OID-name</em></span></span></dt><dd><p>
         (o) The reference name of the OID for
         the variant set, if one is required. The reference names can be found
         in the <span class="emphasis"><em>util</em></span> module of <span class="emphasis"><em><span class="application">YAZ</span></em></span>.
        </p></dd><dt><span class="term">class <span class="emphasis"><em>integer class-name</em></span></span></dt><dd><p>
         (m,r) Introduces a new
         class to the variant set.
        </p></dd><dt><span class="term">type <span class="emphasis"><em>integer type-name datatype</em></span></span></dt><dd><p>
         (m,r) Addes a
         new type to the current class (the one introduced by the most recent
         <span class="emphasis"><em>class</em></span> directive).
         The type names belong to the same name space as the one used
         in the tag set definition file.
        </p></dd></dl></div><p>
    </p><p>
     The following is an excerpt from the file describing the variant set
     <span class="emphasis"><em>Variant-1</em></span>.
    </p><p>

     </p><pre class="screen">
      name variant-1
      reference Variant-1

      class 1 variantId

      type	1	variantId		octetstring

      class 2 body

      type	1	iana			string
      type	2	z39.50			string
      type	3	other			string
     </pre><p>

    </p></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="grs-est-files"></a>3.7. The Element Set (.est) Files</h3></div></div></div><p>
     The element set specification files describe a selection of a subset
     of the elements of a database record. The element selection mechanism
     is equivalent to the one supplied by the <span class="emphasis"><em>Espec-1</em></span>
     syntax of the <acronym class="acronym">Z39.50</acronym> specification.
     In fact, the internal representation of an element set
     specification is identical to the <span class="emphasis"><em>Espec-1</em></span> structure,
     and we'll refer you to the description of that structure for most of
     the detailed semantics of the directives below.
    </p><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><h3 class="title">Note</h3><p>
      Not all of the Espec-1 functionality has been implemented yet.
      The fields that are mentioned below all work as expected, unless
      otherwise is noted.
     </p></div><p>
     The directives available in the element set file are as follows:
    </p><p>
     </p><div class="variablelist"><dl class="variablelist"><dt><span class="term">defaultVariantSetId <span class="emphasis"><em>OID-name</em></span></span></dt><dd><p>
         (o) If variants are used in
         the following, this should provide the name of the variantset used
         (it's not currently possible to specify a different set in the
         individual variant request). In almost all cases (certainly all
         profiles known to us), the name
         <code class="literal">Variant-1</code> should be given here.
        </p></dd><dt><span class="term">defaultVariantRequest <span class="emphasis"><em>variant-request</em></span></span></dt><dd><p>
         (o) This directive
         provides a default variant request for
         use when the individual element requests (see below) do not contain a
         variant request. Variant requests consist of a blank-separated list of
         variant components. A variant component is a comma-separated,
         parenthesized triple of variant class, type, and value (the two former
         values being represented as integers). The value can currently only be
         entered as a string (this will change to depend on the definition of
         the variant in question). The special value (@) is interpreted as a
         null value, however.
        </p></dd><dt><span class="term">simpleElement
        <span class="emphasis"><em>path ['variant' variant-request]</em></span></span></dt><dd><p>
         (o,r) This corresponds to a simple element request
         in <span class="emphasis"><em>Espec-1</em></span>.
         The path consists of a sequence of tag-selectors, where each of
         these can consist of either:
        </p><p>
         </p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p>
            A simple tag, consisting of a comma-separated type-value pair in
            parenthesis, possibly followed by a colon (:) followed by an
            occurrences-specification (see below). The tag-value can be a number
            or a string. If the first character is an apostrophe ('), this
            forces the value to be interpreted as a string, even if it
            appears to be numerical.
           </p></li><li class="listitem"><p>
            A WildThing, represented as a question mark (?), possibly
            followed by a colon (:) followed by an occurrences
            specification (see below).
           </p></li><li class="listitem"><p>
            A WildPath, represented as an asterisk (*). Note that the last
            element of the path should not be a wildPath (wildpaths don't
            work in this version).
           </p></li></ul></div><p>

        </p><p>
         The occurrences-specification can be either the string
         <code class="literal">all</code>, the string <code class="literal">last</code>, or
         an explicit value-range. The value-range is represented as
         an integer (the starting point), possibly followed by a
         plus (+) and a second integer (the number of elements, default
         being one).
        </p><p>
         The variant-request has the same syntax as the defaultVariantRequest
         above. Note that it may sometimes be useful to give an empty variant
         request, simply to disable the default for a specific set of fields
         (we aren't certain if this is proper <span class="emphasis"><em>Espec-1</em></span>,
         but it works in this implementation).
        </p></dd></dl></div><p>
    </p><p>
     The following is an example of an element specification belonging to
     the GILS profile.
    </p><p>

     </p><pre class="screen">
      simpleelement (1,10)
      simpleelement (1,12)
      simpleelement (2,1)
      simpleelement (1,14)
      simpleelement (4,1)
      simpleelement (4,52)
     </pre><p>

    </p></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="schema-mapping"></a>3.8. The Schema Mapping (.map) Files</h3></div></div></div><p>
     Sometimes, the client might want to receive a database record in
     a schema that differs from the native schema of the record. For
     instance, a client might only know how to process WAIS records, while
     the database record is represented in a more specific schema, such as
     GILS. In this module, a mapping of data to one of the <acronym class="acronym">MARC</acronym> formats is
     also thought of as a schema mapping (mapping the elements of the
     record into fields consistent with the given <acronym class="acronym">MARC</acronym> specification, prior
     to actually converting the data to the ISO2709). This use of the
     object identifier for <acronym class="acronym">USMARC</acronym> as a schema identifier represents an
     overloading of the OID which might not be entirely proper. However,
     it represents the dual role of schema and record syntax which
     is assumed by the <acronym class="acronym">MARC</acronym> family in <acronym class="acronym">Z39.50</acronym>.
    </p><p>
     These are the directives of the schema mapping file format:
    </p><p>
     </p><div class="variablelist"><dl class="variablelist"><dt><span class="term">targetName <span class="emphasis"><em>name</em></span></span></dt><dd><p>
         (m) A symbolic name for the target schema
         of the table. Useful mostly for diagnostic purposes.
        </p></dd><dt><span class="term">targetRef <span class="emphasis"><em>OID-name</em></span></span></dt><dd><p>
         (m) An OID name for the target schema.
         This is used, for instance, by a server receiving a request to present
         a record in a different schema from the native one.
         The name, again, is found in the <span class="emphasis"><em>oid</em></span>
         module of <span class="emphasis"><em><span class="application">YAZ</span></em></span>.
        </p></dd><dt><span class="term">map <span class="emphasis"><em>element-name target-path</em></span></span></dt><dd><p>
         (o,r) Adds
         an element mapping rule to the table.
        </p></dd></dl></div><p>
    </p></div><div class="section"><div class="titlepage"><div><div><h3 class="title"><a name="grs-mar-files"></a>3.9. The <acronym class="acronym">MARC</acronym> (ISO2709) Representation (.mar) Files</h3></div></div></div><p>
     This file provides rules for representing a record in the ISO2709
     format. The rules pertain mostly to the values of the constant-length
     header of the record.
    </p></div></div><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="grs-internal-representation.html">Prev</a> </td><td width="20%" align="center"><a accesskey="u" href="grs.html">Up</a></td><td width="40%" align="right"> <a accesskey="n" href="grs-exchange-formats.html">Next</a></td></tr><tr><td width="40%" align="left" valign="top">2. <acronym class="acronym">GRS-1</acronym> Internal Record Representation </td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right" valign="top"> 4. <acronym class="acronym">GRS-1</acronym> Exchange Formats</td></tr></table></div></body></html>
